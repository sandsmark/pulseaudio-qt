/*
    SPDX-FileCopyrightText: 2014-2015 Harald Sitter <sitter@kde.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#include "stream.h"
#include "context_p.h"
#include "stream_p.h"
#include "volumeobject_p.h"

namespace PulseAudioQt
{
Stream::Stream(QObject *parent)
    : VolumeObject(parent)
    , d(new StreamPrivate(this))
{
    VolumeObject::d->m_volumeWritable = false;
    VolumeObject::d->m_hasVolume = false;
}

Stream::~Stream()
{
    delete d;
}

StreamPrivate::StreamPrivate(Stream *q)
    : q(q)
    , m_deviceIndex(PA_INVALID_INDEX)
    , m_clientIndex(PA_INVALID_INDEX)
    , m_virtualStream(false)
    , m_corked(false)
{
}

StreamPrivate::~StreamPrivate()
{
}

QString Stream::name() const
{
    return d->m_name;
}

Client *Stream::client() const
{
    return context()->d->m_clients.data().value(d->m_clientIndex, nullptr);
}

bool Stream::isVirtualStream() const
{
    return d->m_virtualStream;
}

quint32 Stream::deviceIndex() const
{
    return d->m_deviceIndex;
}

bool Stream::isCorked() const
{
    return d->m_corked;
}

} // PulseAudioQt
